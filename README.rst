ToA Analysis Tools
==================

The ToA analysis tools is a package that includes a set of analysis and plot scripts that can be run to
Analyse the Data produced by ToA calibration procedures. It can be installed via `pip` and provides the
`toa-analysis-tools` command line utility for running the various analyses with a whole load of options
run `toa-analysis-tools --help` for details.
