import click
import pandas as pd
import tables
import numpy as np
from rich import print
from rich.progress import track
import glob
from pathlib import Path
import os
import yaml

# from functools import reduce, partial
# from operator import or_
# import multiprocessing
import pickle
from toa_plotting import plot_ctdc_inl
from toa_plotting import plot_ftdc_inl
from toa_plotting import plot_relative_ctdc_occupancy
from toa_plotting import plot_ftdc_correations

CTDC_CALIB_CONFIG_FILE = "ctdc_calib_config_patch.yaml"
FULL_CTDC_PARAMETERS = "ctdc_slopes.pkl"
FTDC_CALIB_CONFIG_FILE = "ftdc_calib_config_patch.yaml"
FULL_FTDC_PARAMETERS = "ftdc_slopes.pkl"
FULL_BIN_EDGES = np.linspace(-0.5, 1023.5, 1025, endpoint=True)
CTDC_BINS_EDGES = np.linspace(-0.5, 255.5, 257, endpoint=True)
FTDC_BINS_EDGES = np.linspace(-0.5, 7.5, 9, endpoint=True)


def generate_gradients_from_files(
    path_list: list, mask_504, tdc_summary_file: Path = None
) -> list:
    inl_gradients = []
    if tdc_summary_file is not None:
        if tdc_summary_file.exists():
            with open(tdc_summary_file, "rb") as fcsf:
                inl_gradients = pickle.load(fcsf)
        else:
            for path in track(path_list, "Analysing INL gradients"):
                print(f"Analysing file {path}")
                inl_gradients += calculate_gradients(path, mask_504=mask_504)
        with open(tdc_summary_file, "wb+") as fcsf:
            pickle.dump(inl_gradients, fcsf)
    else:
        for path in track(path_list, "Analysing INL gradients"):
            inl_gradients += calculate_gradients(path, mask_504=mask_504)
    return inl_gradients


def load_datenraffinerie_data(filename: str):
    tb = tables.open_file(filename)
    df = pd.DataFrame.from_records(tb.root.data.measurements.read())
    tb.close()
    return df


def calculate_toa_histograms(data: pd.DataFrame, mask_tdc_code_504: bool = False):

    ctdc_inl_gradients = []
    chips = data.chip.drop_duplicates()
    for chip in chips:
        chip_d = data.loc[data.chip == chip]
        events_with_data = chip_d.loc[chip_d.toa > 0].event.drop_duplicates()
        channels_with_data = chip_d.loc[chip_d.event.isin(events_with_data)][
            ["half", "channel"]
        ].drop_duplicates()
        if len(channels_with_data) == 0:
            print("The File does not contain channels that have data is empty")
            continue
        for (h, ch) in map(
            lambda x: (int(x[1][0]), int(x[1][1])
                       ), channels_with_data.iterrows()
        ):
            ch_data = chip_d.loc[
                chip_d.event.isin(events_with_data)
                & (chip_d.half == h)
                & (chip_d.channel == ch)
            ]

            if len(ch_data) == 0:
                continue
            base = pd.Series(0, index=np.arange(1024))
            base += ch_data.pivot_table(index="toa", aggfunc=len)["tot"]
            toa_count = base.fillna(0).values
            if sum(toa_count[1:]) == 0:
                print("continue")
                continue

            DAC_CAL_CTDC_L = ch_data.DAC_CAL_CTDC_TOA.drop_duplicates()
            assert len(DAC_CAL_CTDC_L) == 1
            DAC_CAL_CTDC = int(DAC_CAL_CTDC_L.to_numpy()[0])

            DAC_CAL_FTDC_L = ch_data["DAC_CAL_FTDC_TOA"].drop_duplicates()
            assert len(DAC_CAL_FTDC_L) == 1
            DAC_CAL_FTDC = int(DAC_CAL_FTDC_L.to_numpy()[0])

            SIG_P_D_L = ch_data["CTRL_IN_SIG_CTDC_P_D"].drop_duplicates()
            assert len(SIG_P_D_L) == 1
            SIG_P_D = int(SIG_P_D_L.to_numpy()[0])

            REF_P_D_L = ch_data["CTRL_IN_REF_CTDC_P_D"].drop_duplicates()
            assert len(REF_P_D_L) == 1
            REF_P_D: int = int(REF_P_D_L.to_numpy()[0])

            FSIG_P_D_L = ch_data["CTRL_IN_SIG_FTDC_P_D"].drop_duplicates()
            assert len(FSIG_P_D_L) == 1
            FSIG_P_D: int = int(FSIG_P_D_L.to_numpy()[0])

            FREF_P_D_L = ch_data["CTRL_IN_REF_FTDC_P_D"].drop_duplicates()
            assert len(FREF_P_D_L) == 1
            FREF_P_D: int = int(FREF_P_D_L.to_numpy()[0])

            full_mean = np.mean(toa_count)

            # mask ToA code 504 if it is exceptionally large
            if mask_tdc_code_504:
                if toa_count[504] > full_mean * 10:
                    toa_count = toa_count.tolist()
                    del toa_count[504]
                    toa_count.insert(504, int(np.mean(toa_count)))
                    toa_count = np.array(toa_count)

            ctdc_hist = sum([
                np.array(toa_count[i * 256: (i + 1) * 256])
                for i in range(len(toa_count) // 256)
            ])
            ftdc_hist = [
                sum(toa_count[i * 8: (i + 1) * 8]) for i in range(len(toa_count) // 8)
            ]
            ctdc_mean = np.mean(ctdc_hist)
            ctdc_stdd = np.std(ctdc_hist)
            part_ctdc_mean = np.mean(ctdc_hist[20:-20])
            part_ctdc_stdd = np.std(ctdc_hist[20:-20])
            ctdc_inl_gradients.append(
                {
                    "ch": ch,
                    "chip": chip,
                    "half": h,
                    "ref": REF_P_D,
                    "sig": SIG_P_D,
                    "fref": FREF_P_D,
                    "fsig": FSIG_P_D,
                    "dac_cal_ctdc": DAC_CAL_CTDC,
                    "dac_cal_ftdc": DAC_CAL_FTDC,
                    "ftdc_hist": ftdc_hist,
                    "slope_diff": float(part_ctdc_mean - ctdc_mean),
                    "part_ctdc_mean": float(part_ctdc_mean),
                    "avg_stdd": float(np.sqrt(ctdc_stdd**2 + part_ctdc_stdd**2)),
                    "full_hist": toa_count.tolist(),
                    "full_inl": np.cumsum(toa_count - full_mean).tolist(),
                    "ctdc_hist": ctdc_hist,
                    "ctdc_inl": np.cumsum(ctdc_hist - ctdc_mean).tolist(),
                }
            )
    return ctdc_inl_gradients


def find_optimal_ref_and_sig_parameter(ctdc_inl_gradients):
    out_dict = {}
    out_dict["target"] = {}
    halves_with_data = set(
        map(lambda x: (x["chip"], x["half"]), ctdc_inl_gradients))

    # calculate the average for all the channels in the half for any
    # given ref, sig parameter combination
    channel_averages = []
    for cp, h in halves_with_data:
        half_gradients = list(
            filter(lambda x: x["chip"] == cp and x["half"]
                   == h, ctdc_inl_gradients)
        )

        params = set(map(lambda x: (x["ref"], x["sig"]), half_gradients))
        for ref, sig in params:
            channel_gradients = list(
                filter(lambda y: y["ref"] ==
                       ref and y["sig"] == sig, half_gradients)
            )
            channel_averages.append(
                {
                    "chip": cp,
                    "half": h,
                    "ref": ref,
                    "sig": sig,
                    "avg_slope": np.mean(
                        list(map(lambda x: x["slope_diff"], channel_gradients))
                    ),
                    "channel_gradients": channel_gradients,
                }
            )
    # find the optimal ref/sig parameter for every half
    optimal_gradients = []
    for cp, h in halves_with_data:
        half_avg_gradients = list(
            filter(lambda x: x["chip"] == cp and x["half"]
                   == h, channel_averages)
        )
        half_avg_gradients = sorted(
            half_avg_gradients, key=lambda x: np.abs(x["avg_slope"])
        )
        opt_ref = half_avg_gradients[0]["ref"]
        opt_sig = half_avg_gradients[0]["sig"]
        optimal_gradients += half_avg_gradients[0]["channel_gradients"]
        try:
            _ = out_dict["target"][f"roc_s{cp}"]
        except KeyError:
            out_dict["target"][f"roc_s{cp}"] = {"MasterTdc": {}}
        out_dict["target"][f"roc_s{cp}"]["MasterTdc"][h] = {
            "CTRL_IN_REF_CTDC_P_EN": 1,
            "CTRL_IN_REF_CTDC_P_D": opt_ref,
            "CTRL_IN_SIG_CTDC_P_EN": 1,
            "CTRL_IN_SIG_CTDC_P_D": opt_sig,
        }
    return out_dict, optimal_gradients


def find_optimal_toa_dac_cal(ctdc_inl_graients):
    channels_with_data = set(
        map(lambda x: (x["chip"], x["half"], x["ch"]), ctdc_inl_graients)
    )
    output_config = {}
    output_config["target"] = {}
    optimal_gradients = []
    for cp, h, ch in channels_with_data:
        print(
            f"Finding optimal CAL_DAC_CTDC_D for channel {ch} in half {h} of "
            f"chip {cp}"
        )
        channel_data = list(
            filter(
                lambda x: x["chip"] == cp and x["half"] == h and x["ch"] == ch,
                ctdc_inl_graients,
            )
        )
        channel_data = sorted(
            channel_data, key=lambda x: np.abs(x["slope_diff"]))
        optimal_dac_cal_ctdc = channel_data[0]["dac_cal_ctdc"]
        optimal_gradients.append(channel_data[0])
        try:
            _ = output_config["target"][f"roc_s{cp}"]
        except KeyError:
            output_config["target"][f"roc_s{cp}"] = {}
            output_config["target"][f"roc_s{cp}"]["ch"] = {}
        if ch < 38:
            try:
                _ = output_config["target"][f"roc_s{cp}"]["ch"][ch + 36 * h]
            except KeyError:
                output_config["target"][f"roc_s{cp}"]["ch"][ch + 36 * h] = {}
            output_config["target"][f"roc_s{cp}"]["ch"][ch + 36 * h][
                "DAC_CAL_CTDC_TOA"
            ] = optimal_dac_cal_ctdc
        if ch == 36:
            rel_chan = ch - 36 + h * 1
            try:
                _ = output_config["target"][f"roc_s{cp}"]["calib"][rel_chan]
            except KeyError:
                output_config["target"][f"roc_s{cp}"]["calib"][rel_chan] = {}
            output_config["target"][f"roc_s{cp}"]["calib"][rel_chan][
                "DAC_CAL_CTDC_TOA"
            ] = optimal_dac_cal_ctdc
        if ch in [37, 38]:
            rel_chan = ch - 37 + h * 2
            try:
                _ = output_config["target"][f"roc_s{cp}"]["cm"][rel_chan]
            except KeyError:
                output_config["target"][f"roc_s{cp}"]["cm"][rel_chan] = {}
            output_config["target"][f"roc_s{cp}"]["cm"][rel_chan][
                "DAC_CAL_CTDC_TOA"
            ] = optimal_dac_cal_ctdc
    return output_config, optimal_gradients


def find_optimal_fsig_fref(inl_gradients):
    out_dict = {}
    out_dict["target"] = {}
    halves_with_data = set(
        map(lambda x: (x["chip"], x["half"]), inl_gradients))
    # calculate the average for all the channels in the half for any
    # given ref, sig parameter combination
    channel_averages = []
    for cp, h in halves_with_data:
        half_gradients = list(
            filter(lambda x: x["chip"] == cp and x["half"] == h, inl_gradients)
        )

        params = set(map(lambda x: (x["fref"], x["fsig"]), half_gradients))
        for ref, sig in params:
            channel_gradients = list(
                filter(lambda y: y["fref"] ==
                       ref and y["fsig"] == sig, half_gradients)
            )
            channel_averages.append(
                {
                    "chip": cp,
                    "half": h,
                    "fref": ref,
                    "fsig": sig,
                    "avg_ftdc_stdd": np.std(
                        list(map(lambda x: x["ftdc_hist"], channel_gradients))
                    ),
                    "channel_gradients": channel_gradients,
                }
            )
    # find the optimal ref/sig parameter for every half
    optimal_gradients = []
    for cp, h in halves_with_data:
        half_avg_gradients = list(
            filter(lambda x: x["chip"] == cp and x["half"]
                   == h, channel_averages)
        )
        half_avg_gradients = sorted(
            half_avg_gradients, key=lambda x: x["avg_ftdc_stdd"]
        )
        opt_ref = half_avg_gradients[0]["fref"]
        opt_sig = half_avg_gradients[0]["fsig"]
        optimal_gradients += half_avg_gradients[0]["channel_gradients"]
        try:
            _ = out_dict["target"][f"roc_s{cp}"]
        except KeyError:
            out_dict["target"][f"roc_s{cp}"] = {"MasterTdc": {}}
        out_dict["target"][f"roc_s{cp}"]["MasterTdc"][h] = {
            "CTRL_IN_REF_FTDC_P_EN": 1,
            "CTRL_IN_REF_FTDC_P_D": opt_ref,
            "CTRL_IN_SIG_FTDC_P_EN": 1,
            "CTRL_IN_SIG_FTDC_P_D": opt_sig,
        }
    return out_dict, optimal_gradients


def calculate_gradients(data_file: Path, mask_504: bool = False):
    data = load_datenraffinerie_data(data_file)
    data.query("toa!=0", inplace=True)
    ctdc_inl_gradients = calculate_toa_histograms(
        data, mask_tdc_code_504=mask_504)
    return ctdc_inl_gradients


@click.group("analysis")
def analysis():
    ...


@analysis.command()
@click.argument(
    "sig-folder", type=click.Path(exists=True, dir_okay=True), metavar="[SIG-DATA-DIR]"
)
@click.argument(
    "ref-folder", type=click.Path(exists=True, dir_okay=True), metavar="[REF-DATA-DIR]"
)
@click.argument("output", type=click.Path(dir_okay=True), metavar="[OUTPUT]")
@click.option(
    "--plot",
    "-p",
    is_flag=True,
    default=False,
    help="generate the plots as well as the calibration",
)
@click.option(
    "--mask-504",
    "-m",
    is_flag=True,
    default=True,
    help="mask unlikely high occurrences of TDC code 504",
)
def ctdc_ref_and_sig(sig_folder, ref_folder, output, plot, mask_504):
    output = Path(output)
    if not output.exists():
        os.mkdir(output)
    data_paths = list(glob.glob(str(sig_folder) + "/*.h5"))
    data_paths += list(glob.glob(str(ref_folder) + "/*.h5"))
    summary_file = output / "ctdc-ref-sig-summary.pkl"
    inl_gradients = generate_gradients_from_files(
        data_paths, mask_504, summary_file)
    out_dict, optimal_gradients = find_optimal_ref_and_sig_parameter(
        inl_gradients)
    with open(output / "trimmed-ctdc-ref-sig-config.yaml", "w+") as cccf:
        cccf.write(yaml.safe_dump(out_dict))
    if plot:
        for grad in track(inl_gradients, "Plotting INL Gradients"):
            plot_ctdc_inl(output, grad, ensure_channel_dir=True)
            plot_relative_ctdc_occupancy(output, grad, ensure_channel_dir=True)
        trimmed_plots = output / "trimmed-ctdc-ref-sig-plots"
        if not trimmed_plots.exists():
            trimmed_plots.mkdir()
        for grad in optimal_gradients:
            plot_ctdc_inl(trimmed_plots, grad, ensure_channel_dir=False)
            plot_relative_ctdc_occupancy(
                trimmed_plots, grad, ensure_channel_dir=False)


@analysis.command()
@click.argument(
    "sig-folder", type=click.Path(exists=True, dir_okay=True), metavar="[FSIG-DATA-DIR]"
)
@click.argument(
    "ref-folder", type=click.Path(exists=True, dir_okay=True), metavar="[FREF-DATA-DIR]"
)
@click.argument("output", type=click.Path(dir_okay=True), metavar="[OUTPUT]")
@click.option(
    "--plot",
    "-p",
    is_flag=True,
    default=False,
    help="generate the plots as well as the calibration",
)
@click.option(
    "--mask-504",
    "-m",
    is_flag=True,
    default=True,
    help="mask unlikely high occurrences of TDC code 504",
)
def ftdc_ref_and_sig(sig_folder, ref_folder, output, plot, mask_504):
    output = Path(output)
    if not output.exists():
        os.mkdir(output)
    data_paths = list(glob.glob(str(sig_folder) + "/*.h5"))
    data_paths += list(glob.glob(str(ref_folder) + "/*.h5"))
    inl_gradients = []
    summary_file = output / "ftdc-ref-sig-summary.pkl"
    inl_gradients = generate_gradients_from_files(
        data_paths, mask_504, summary_file)
    out_dict, optimal_gradients = find_optimal_fsig_fref(inl_gradients)
    with open(output / "trimmed-ftdc-ref-sig-config.yaml", "w+") as cccf:
        cccf.write(yaml.safe_dump(out_dict))
    if plot:
        for grad in track(inl_gradients, "Plotting INL Gradients"):
            plot_ftdc_inl(output, grad, ensure_channel_dir=True)
            plot_ftdc_correations(output, grad, ensure_channel_dir=True)
        trimmed_plots = output / "trimmed-ftdc-ref-sig-plots"
        if not trimmed_plots.exists():
            trimmed_plots.mkdir()
        for grad in optimal_gradients:
            plot_ftdc_inl(trimmed_plots, grad, ensure_channel_dir=False)
            plot_ftdc_correations(trimmed_plots, grad,
                                  ensure_channel_dir=False)


@analysis.command()
@click.argument("channel-wise-folder", type=click.Path(exists=True, dir_okay=True))
@click.argument("output", type=click.Path(dir_okay=True), metavar="[OUTPUT]")
@click.option(
    "--plot",
    "-p",
    is_flag=True,
    default=False,
    help="generate the plots as well as the calibration",
)
@click.option(
    "--mask-504",
    "-m",
    is_flag=True,
    default=True,
    help="mask unlikely high occurrences of TDC code 504",
)
def channel_wise_ctdc_toa(channel_wise_folder, output, plot, mask_504):
    output = Path(output)
    if not output.exists():
        os.mkdir(output)
    data_paths = list(glob.glob(str(channel_wise_folder) + "/*.h5"))
    summary_file = output / "channel-wise-ctdc-summary.pkl"
    inl_gradients = generate_gradients_from_files(
        data_paths, mask_504, summary_file)
    out_dict, optimal_gradients = find_optimal_toa_dac_cal(inl_gradients)
    with open(output / "trimmed-channel-wise-ctdc-config.yaml", "w+") as cccf:
        cccf.write(yaml.safe_dump(out_dict))
    if plot:
        for grad in track(inl_gradients, "Plotting INL Gradients"):
            plot_ctdc_inl(output, grad, ensure_channel_dir=True)
            plot_relative_ctdc_occupancy(output, grad, ensure_channel_dir=True)
        trimmed_plots = output / "trimmed-channel-wise-ctdc-plots"
        if not trimmed_plots.exists():
            trimmed_plots.mkdir()
        for grad in optimal_gradients:
            plot_ctdc_inl(trimmed_plots, grad, ensure_channel_dir=False)
            plot_relative_ctdc_occupancy(
                trimmed_plots, grad, ensure_channel_dir=False)


@analysis.command()
@click.argument("channel-wise-folder", type=click.Path(exists=True, dir_okay=True))
@click.argument("output", type=click.Path(dir_okay=True), metavar="[OUTPUT]")
@click.option(
    "--plot",
    "-p",
    is_flag=True,
    default=False,
    help="generate the plots as well as the calibration",
)
@click.option(
    "--mask-504",
    "-m",
    is_flag=True,
    default=True,
    help="mask unlikely high occurrences of TDC code 504",
)
def channel_wise_ftdc_toa(channel_wise_folder, output, plot, mask_504):
    output = Path(output)
    if not output.exists():
        os.mkdir(output)
    data_paths = list(glob.glob(str(channel_wise_folder) + "/*.h5"))
    summary_file = output / "channel-wise-ftdc-summary.pkl"
    inl_gradients = generate_gradients_from_files(
        data_paths, mask_504, summary_file)
    out_dict, optimal_gradients = find_optimal_toa_dac_cal(inl_gradients)
    print("writing calibration patch")
    with open(output / "trimmed-channel-wise-ftdc-config.yaml", "w+") as cccf:
        cccf.write(yaml.safe_dump(out_dict))
    if plot:
        for grad in track(inl_gradients, "Plotting INL Gradients"):
            plot_ftdc_inl(output, grad, ensure_channel_dir=True)
            plot_ftdc_correations(output, grad, ensure_channel_dir=True)
    trimmed_plots = output / "trimmed-channel-wise-ftdc-plots"
    if not trimmed_plots.exists():
        trimmed_plots.mkdir()
    for grad in optimal_gradients:
        plot_ftdc_inl(trimmed_plots, grad, ensure_channel_dir=False)
        plot_ftdc_correations(trimmed_plots, grad, ensure_channel_dir=False)


if __name__ == "__main__":
    analysis()
