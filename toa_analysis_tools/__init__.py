"""
Contain all the command line tools to produce the various plots and configurations
related to the ToA of the HGCROCv3
"""
__version__ = '0.0.1'
