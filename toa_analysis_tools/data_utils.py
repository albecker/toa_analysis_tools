"""
Module containing data handling routines
tailored to the ToA data analysis
"""
import pandas as pd
import numpy as np


def remove_channel_misfires(frame):
    """
    With some chip halfs the following seems to occurr:
    1) All channels except for one trigger
    2) Only one channel triggers
    To accurately estimate the trimming parameters these
    Events must be removed from the data
    """
    def is_zero(num):
        if num == 0:
            return 1
        return 0
    zeros_in_event = frame.applymap(is_zero).sum(axis=1)
    return frame.loc[(zeros_in_event != 1) & (zeros_in_event != 8), :]
