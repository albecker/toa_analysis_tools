import pickle
import logging
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
from pathlib import Path
import numpy as np
import click

mpl.use('qtagg')
# dictionary that converts between string and debug level
log_level_dict = {'DEBUG': logging.DEBUG,
                  'INFO': logging.INFO,
                  'WARNING': logging.WARNING,
                  'ERROR': logging.ERROR,
                  'CRITICAL': logging.CRITICAL}


def count_events(dataframe):
    logger = logging.getLogger('event-count')
    dataframe = dataframe.unstack(level=[-1, -2, -3])
    logger.info('Table restored')
    acg_settings = dataframe.index.droplevel(-1).drop_duplicates()
    logger.info('ACG settings: %s' % list(acg_settings))
    chip_halves = dataframe.columns.droplevel(-1).drop_duplicates()
    logger.info('Found half-chips: %s' % list(chip_halves))
    events_in_file = []
    for half_chip in chip_halves:
        event_counts = [(half_chip, setting, len(
            dataframe.loc[setting, half_chip])) for setting in acg_settings]
        logging.info("adding %d entries to the event counts" %
                     len(event_counts))
        events_in_file += event_counts
    return events_in_file


@click.command('acg-rate-tuning-plot')
@click.argument('data-folder', type=click.Path(dir_okay=True, exists=True))
@click.option('-p', '--pickle-file',
              type=click.File(mode='rb'), default=None,
              help='File storing the intermediary data used for plotting')
@click.option('-n', '--plot-name', type=str, default='acg-hit-rate-plot.pdf',
              help='Name of the file that the plot should be saved as')
@click.option('-s', '--show', is_flag=True, default=False, show_default=True,
              help='Flag that shows the plot on the screen when set')
@click.option('-w', '--width', type=float, default=8,
              help='width of the figure in inches')
@click.option('-h', '--height', type=float, default=5,
              help='height of the figure in inches')
@click.option("-l", "--log", type=click.Path(),
              default='plot-acg-rates.log',
              help="file to write the logs to")
@click.option('-v', "--loglevel", type=click.Choice(log_level_dict.keys()),
              default="INFO", show_default=True,
              help="Set the loglevel")
def acg_rate_tuning_plot(data_folder, plot_name, pickle_file, show,
                         width, height,
                         log, loglevel):
    logging.basicConfig(filename=log, level=log_level_dict[loglevel],
                        filemode='w+',
                        format='[%(asctime)s] %(levelname)-10s:'
                               '%(name)-30s %(message)s')
    logger = logging.getLogger('plot-data-processing')
    if pickle_file is None:
        data_folder = Path(data_folder)
        data_files = list(data_folder.glob('*.h5'))
        logger.info("Found data files %s" % data_files)
        events_per_setting = []
        for file in data_files:
            logger.info('Reading in Data')
            data = pd.read_hdf(file, 'table')
            logger.info('Data loaded')
            events_per_setting += count_events(data)
        pkl_output_file = 'acg-tuning-data.pkl'
        logger.info("writing data out to intermediate file %s" %
                    pkl_output_file)
        with open(pkl_output_file, 'wb') as pklf:
            pickle.dump(events_per_setting, pklf)
    else:
        events_per_setting = pickle.load(pickle_file)

    fig, ax = plt.subplots(figsize=(width, height))
    cmap = mpl.colormaps['viridis']
    acg_gains = set(map(lambda x: x[1][0], events_per_setting))
    logger.info('acg_gains: %s' % acg_gains)
    # now we want to get the average and the standard deviation for each rcg_gain from the values for all halves
    for acg_gain in acg_gains:
        counts = list(map(lambda x: (x[1][1], x[2]), filter(
            lambda x: x[1][0] == acg_gain, events_per_setting)))
        vco_vals = list(map(lambda x: x[0], counts))
        event_cnt = list(map(lambda x: x[1]/250000, counts))
        #logger.info('vco_settings for acg_gain %d: %s' % (acg_gain, vco_vals))
        # means = [np.mean(list(filter(lambda x: x[0] == vco_val, counts)))
        #         for vco_val in vco_vals]
        # uncertainties = [
        #    np.std(list(filter(lambda x: x[0] == vco_val, counts))) for vco_val in vco_vals]
        # logger.info('uncertainties for acg gain %d: %s' %
        #            (acg_gain, uncertainties))
        ax.scatter(vco_vals, event_cnt,
                   label=f'ACG-gain = {acg_gain}', marker='v',
                   color=cmap((acg_gain / max(acg_gains)) * 0.7 + 0.2))
    ax.legend()
    ax.set_xlabel('VCO-DAC-Code')
    ax.set_ylabel('Fraction of events containing triggered ToAs')
    fig.savefig(plot_name)
    if show:
        plt.show()
    plt.close(fig)


if __name__ == "__main__":
    acg_rate_tuning_plot()
