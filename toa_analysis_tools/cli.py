"""
Module containing the main command line applications that
are available to the user
"""
import logging
import sys
from pathlib import Path
from . import __version__
import click
from .utils import set_up_logger
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from .extract_toa_data_from_drf_data import level_from_name
from .extract_toa_data_from_drf_data import extract_toa_data
from .plot_ctdc_optimal_sig_ref_search import plot_optimal_ctdc_search
from .data_utils import remove_channel_misfires


log_level_dict = {'DEBUG': logging.DEBUG,
                  'INFO': logging.INFO,
                  'WARNING': logging.WARNING,
                  'ERROR': logging.ERROR,
                  'CRITICAL': logging.CRITICAL}

PLOT_SUFFIXES = ['.pdf', ".svg", ".png", ".jpg"]


@click.group()
@click.option('-v', '--verbose', count=True,
              help='make the program be more verbose about what it is doing')
@click.option('-fl', '--loglevel',
              type=click.Choice(log_level_dict.keys(),
                                case_sensitive=False),
              default='INFO',
              help='Set the loglevel for the log file')
@click.version_option(__version__)
@click.pass_context
def cli(ctx, verbose,
        loglevel: int):
    """
    Tools to process and create plots for data from the ToA calibration and
    characterisation:
    """
    # in this function we can do all the things that would need doing
    # for every invocation of the tool, but nothing other than delegating
    # the actual tasks to commands
    ctx.obj = {'verbosity': verbose,
               'loglevel': loglevel}


@cli.command('process-Master-CTDC-trim-data')
@click.argument("sig-scan-directory",
                type=click.Path(dir_okay=True, file_okay=False, exists=True))
@click.argument("ref-scan-directory",
                type=click.Path(dir_okay=True, file_okay=False, exists=True))
@click.argument("output-file",
                type=click.Path(dir_okay=True, file_okay=False, exists=True))
@click.option('-l', '--logfile',
              type=click.Path(dir_okay=False, file_okay=True),
              default='./process-reference-ctdc-data.log',
              help='specify the location of the logfile',
              show_default=True)
@click.pass_context
def process_datenraffinerie_sig_ref_data(ctx, sig_scan_directory,
                                         ref_scan_directory, output_file, logfile):
    """
    process the data produced by the datenraffinerie, stored in the
    `sig-scan-directory` and `ref-scan-directory.
    Write the output to `output-file`

    This program processes the data coming from the
    datenraffinerie by performing the following steps:
        1) filter out all events without a ToA trigger
        2) split the table into a single table per half-roc
        2) reformat the data such that the columns of the data
           contain the channels and the index of the table contains
           the settings under wich the data was acquired
        3) store every half-roc table as a separate group into the
           output hdf-5 file named the groups are named as
           chip-${chip}-half-${half}
    """
    logger = logging.getLogger('proc-datenraf-data')
    set_up_logger(logger, verbosity_level=ctx.obj['verbosity'],
                  loglevel=ctx.obj['loglevel'], logfile=logfile)
    sig_scan_directory = Path(sig_scan_directory)
    ref_scan_directory = Path(ref_scan_directory)
    cleaned_hw_data = []
    data_files = list(sig_scan_directory.glob('*.h5')) + \
        list(ref_scan_directory.glob('*.h5'))
    data_files = sorted(data_files)
    for file in data_files:
        logger.info(f"loading data from {file.absolute()}")
        data = load_datenraffinerie_data(file)
        logger.info(f"Extracting ToA data from {file.absolute()}")
        cleaned_hw_data = extract_toa_data(data)
        chip_halves = set(list(map(lambda e: (e[0], e[1]), cleaned_hw_data)))
        for chip, half in chip_halves:
            hw_data = list(map(
                lambda d: d[-1], filter(lambda d: d[0] == chip and d[1] == half, cleaned_hw_data)))
            assert len(hw_data) == 1
            group_name = f'chip-{chip}-half-{half}'
            logger.info(f"Writing Data for chip {chip} half {half} to disk.")
            hw_data[-1].stack(level=[-1, -2, -3]).astype('uint32').to_hdf(
                output_file, group_name, format='table', append=True)


@cli.command()
@click.argument('data-file', type=click.Path(dir_okay=False, exists=True))
@click.argument('chip', type=int)
@click.argument('half', type=click.IntRange(0, 1))
@click.argument('channel', type=click.IntRange(0, 39))
@click.option('-s', '--sig', type=click.IntRange(0, 31), default=slice(None),
              help='set a Value for the CTRL_IN_SIG_CTDC_P_D')
@click.option('-r', '--ref', type=click.IntRange(0, 31), default=slice(None))
@click.option('-d', '--dac', type=click.IntRange(0, 31), default=slice(None))
@click.option('-o', '--output', type=click.Path(dir_okay=False), default=None,
              help='file to write the plot to (please specify filetype)')
@click.option('-l', '--logfile',
              type=click.Path(dir_okay=False, file_okay=True),
              default='./process-reference-ctdc-data.log',
              help='specify the location of the logfile',
              show_default=True)
@click.option('-p', "--show-plot/--no-show", is_flag=True, default=True, show_default=True,
              help="Show the plot interactively")
@click.option('-c', '--clean/--no-clean', is_flag=True, default=False, show_default=True,
              help="Remove the events where only a single channel is "
                   "0 or only a single event is not 0")
@click.pass_context
def toa_histogram(ctx, data_file, chip, half, channel, sig, ref, dac,
                  output, logfile, show_plot, clean):
    """
    Plot the histogram of ToA codes for a given chip and channel
    """
    # set up
    logger = logging.getLogger('toa-histogram')
    set_up_logger(logger, verbosity_level=ctx.obj['verbosity'],
                  loglevel=ctx.obj['loglevel'], logfile=Path(logfile))
    logger.info('Reading data from %s' % data_file)

    # check if the output is set
    if output is not None:
        output = Path(output)
        if output.suffix not in PLOT_SUFFIXES:
            logger.error("Output file name %s does not end in %s" %
                         (output.suffix, PLOT_SUFFIXES))

    # load data
    data = pd.read_hdf(data_file)
    data = data.unstack(level=[-1, -2, -3])

    # clean the data from channel misfires
    if clean:
        data = remove_channel_misfires(data)

    # select the right data
    sig_index = level_from_name(data.index, "CTRL_IN_SIG_CTDC_P_D")
    if sig_index is None:
        msg = "CTRL_IN_SIG_CTDC_P_D Can not be used to index the Dataset"
        logger.critical(msg)
        sys.exit(1)
    ref_index = level_from_name(data.index, "CTRL_IN_REF_CTDC_P_D")
    if ref_index is None:
        msg = "CTRL_IN_REF_CTDC_P_D Can not be used to index the Dataset"
        logger.critical(msg)
        sys.exit(1)
    dac_index = level_from_name(data.index, 'DAC_CAL_CTDC_TOA')
    if dac_index is None:
        msg = "DAC_CAL_CTDC_TOA can not be used to index the Dataset"
        logger.critical(msg)
        sys.exit(1)
    index_slice = []
    for i, _ in enumerate(data.index.names):
        if i == sig_index:
            index_slice.append(sig)
            continue
        if i == ref_index:
            index_slice.append(ref)
            continue
        if i == dac_index:
            index_slice.append(dac)
        index_slice.append(slice(None))
    logger.info("Selecting relevant data subset")
    subframe = data.loc[tuple(index_slice), (chip, half, channel)]
    logger.debug("%s" % subframe)

    # create histogram
    zero_frame = pd.Series(np.zeros(1024), name=(
        chip, half, channel))
    toa_hist = (zero_frame + subframe.value_counts()
                ).fillna(0).astype(int).sort_index()
    # create plot
    fig, axes = plt.subplots(figsize=(8, 6))
    axes.hist(np.arange(0, 1024), bins=(
        np.arange(0, 1025) - .5), weights=toa_hist.values)
    axes.set_title(
        f"ToA hist for SIG={sig}, REF={ref}, DAC={dac}, Chip={chip}, Half={half}, Channel={channel}")
    axes.set_ylabel('Code Occupancy')
    axes.set_xlabel('ToA Code')

    if output is not None:
        fig.savefig(output)
    if show_plot:
        plt.show()


cli.add_command(plot_optimal_ctdc_search, name="plot-ctdc-trim")


@cli.command()
@click.argument('data-file', type=click.Path(dir_okay=False, exists=True))
@click.argument('chip', type=int)
@click.argument('half', type=click.IntRange(0, 1))
@click.argument('channel', type=click.IntRange(0, 39))
@click.argument('threshold', type=click.IntRange(0, max_open=True))
@click.option('-s', '--sig', type=click.IntRange(0, 31), default=slice(None),
              help='set a Value for the CTRL_IN_SIG_CTDC_P_D')
@click.option('-r', '--ref', type=click.IntRange(0, 31), default=slice(None))
@click.option('-d', '--dac', type=click.IntRange(0, 31), default=slice(None))
def find_high_occupancy_codes(data_file, chip, half, channel, threshold, sig, ref, dac):
    """
    Given a channel and a set of (optional) TDC settings, find all ToA codes
    that have an occupancy above the user defined threshold
    """
    # load the data
    data = pd.read_hdf(data_file)
    data = data.unstack(level=[-1, -2, -3])

    # select the proper subset of the data
    sig_level = level_from_name(data.index, 'CTRL_IN_SIG_CTDC_P_D')
    ref_level = level_from_name(data.index, 'CTRL_IN_REF_CTDC_P_D')
    dac_level = level_from_name(data.index, 'DAC_CAL_CTDC_TOA')
    row_selection = []
    for i, _ in enumerate(data.columns.names):
        if i == sig_level:
            row_selection.append(sig)
        elif i == ref_level:
            row_selection.append(ref)
        elif i == dac_level:
            row_selection.append(dac)
        else:
            row_selection.append(slice(None))
    if (chip, half, channel) not in data.columns.drop_duplicates():
        click.echo(
            f"The channel {channel} in half {half} of chip {chip} "
            "can't be found in the data")
        click.echo("Available channels:")
        click.echo("Chip\tHalf\tChannel")
        for chip, half, chan in data.columns.drop_duplicates():
            click.echo(f"{chip:>4}\t{half:>4}\t{chan:>7}")
        sys.exit()
    try:
        sframe = data.loc[tuple(row_selection), (chip, half, channel)]
    except KeyError:
        # if the subset is empty,
        click.echo("No data found for the selected settings.")
        chan_data = data.loc[:, (chip, half, channel)]
        available_settings = chan_data.index.droplevel(
            'event').drop_duplicates()
        click.echo("Available settings:")
        setting_names = chan_data.index.droplevel('event').names
        max_name_length = max(map(len, setting_names))
        for i, name in enumerate(setting_names):
            click.echo(f"{name:>{max_name_length}}\t", nl=False)
        click.echo()
        for setting in available_settings:
            for val in setting:
                click.echo(f"{val:>{max_name_length}d}\t", nl=False)
            click.echo("")
        sys.exit()
    bin_counts = sframe.value_counts()
    bins_above_threshold = bin_counts.loc[bin_counts >
                                          threshold].index
    click.echo(
        f"The channel ({chip}, {half}, {channel}) was hit a total of {bin_counts.sum()} times")
    click.echo(f"{len(bins_above_threshold)} Channels above {threshold}:")
    for toa_bin in sorted(bins_above_threshold):
        click.echo(f"\t{toa_bin:>5} with {bin_counts[toa_bin]:>7} events")


@ cli.command()
@ click.argument("data-file", type=click.Path(dir_okay=False, exists=True))
@ click.option('-g', "--group", type=str,
               help="The group inside the hdf-5 file that the data is stored in")
def show_available_settings(data_file, group):
    """
    Show the settings contained in the data set grouped by channel
    """
    data = pd.read_hdf(data_file, group)
    data.unstack(level=[-1, -2, -3])
    chips = level_from_name(data.columns, 'chip')
    if chips is None:
        click.echo('No chip found in data')
    else:
        click.echo()
    chan_data = data.loc[:, (chip, half, channel)]
    available_settings = chan_data.index.droplevel('event').drop_duplicates()
    for setting in available_settings:
        for name in chan_data.index.droplevel('event').names:
            pass
