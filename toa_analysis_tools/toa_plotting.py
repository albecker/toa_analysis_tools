import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import figure
from pathlib import Path
import numpy as np
from utils import select_ftdc_dist_from_ctdc_bin
from utils import ensure_channel_output_dir
from utils import prepare_data_for_ctdc_comparison

FULL_BIN_EDGES = np.linspace(-.5, 1023.5, 1025, endpoint=True)
CTDC_BINS_EDGES = np.linspace(-.5, 255.5, 257, endpoint=True)
FTDC_BINS_EDGES = np.linspace(-.5, 7.5, 9, endpoint=True)


def make_ftdc_inl_gradient_plot(extracted_data_element: dict) -> figure:
    cp = extracted_data_element['chip']
    ch = extracted_data_element['ch']
    h = extracted_data_element['half']
    fref = extracted_data_element['fref']
    fsig = extracted_data_element['fsig']
    dac_cal_ftdc = extracted_data_element['dac_cal_ftdc']
    ftdc_hist = extracted_data_element['ftdc_hist']
    hist = extracted_data_element['full_hist']
    sig_p_d = extracted_data_element['sig']
    ref_p_d = extracted_data_element['ref']
    dac_cal_ctdc = extracted_data_element['dac_cal_ctdc']
    fig, axes = plt.subplots(1, 2, figsize=(2*8, 1*8))
    fig.suptitle(f'ToA Fine TDC for Chip={cp}, Half={h}, Channel={ch}\n'
                 f'CTRL_IN_SIG_CTDC_P_D = {sig_p_d}, '
                 f'CTRL_IN_REF_CTDC_P_D = {ref_p_d}, '
                 f'DAC_CAL_CTDC_TOA = {dac_cal_ctdc}\n'
                 f'CTRL_IN_SIG_FTDC_P_D = {fsig}, '
                 f'CTRL_IN_REF_FTDC_P_D = {fref}, '
                 f'DAC_CAL_FTDC_TOA = {dac_cal_ftdc}')
    ToA_hist_ax = axes[0]
    FTDC_hist_ax = axes[1]
    ToA_hist_ax.hist(FULL_BIN_EDGES[:-1],
                     bins=FULL_BIN_EDGES,
                     weights=hist,
                     alpha=1,
                     stacked=False,
                     fill=False,
                     histtype='step')
    FTDC_hist_ax.hist(FTDC_BINS_EDGES[:-1],
                      bins=FTDC_BINS_EDGES,
                      weights=ftdc_hist,
                      alpha=1,
                      stacked=False,
                      fill=False,
                      histtype='step')
    ToA_hist_ax.set_ylim((0, 120))
    ToA_hist_ax.set_xlabel('ToA Code')
    ToA_hist_ax.set_ylabel('Occurrence')
    ToA_hist_ax.set_title(
        f'ToA-TDC code distribution for channel {ch} of chip {cp}, half {h}')
    FTDC_hist_ax.set_ylabel('Count')
    FTDC_hist_ax.set_xlabel('ToA-FTDC code')
    FTDC_hist_ax.grid()
    FTDC_hist_ax.set_title('ToA FTDC code distribution')
    return fig


def make_ctdc_inl_gradient_plot(extracted_data_element: dict) -> figure:
    sig_p_d = extracted_data_element['sig']
    ref_p_d = extracted_data_element['ref']
    dac_cal_ctdc = extracted_data_element['dac_cal_ctdc']
    ch = extracted_data_element['ch']
    cp = extracted_data_element['chip']
    h = extracted_data_element['half']
    hist = extracted_data_element['full_hist']
    ctdc_hist = extracted_data_element['ctdc_hist']
    ctdc_inl = extracted_data_element['ctdc_inl']
    full_inl = extracted_data_element['full_inl']
    part_ctdc_mean = extracted_data_element['part_ctdc_mean']
    ctdc_slope_diff = extracted_data_element['slope_diff']

    fig, axes = plt.subplots(2, 2, figsize=(2*8, 2*8))
    fig.suptitle(f'ToA Master TDC data Channel = {ch}, Chip = {cp}-{h} \n'
                 f'CTRL_IN_SIG_CTDC_P_D = {sig_p_d}\n'
                 f'CTRL_IN_REF_CTDC_P_D = {ref_p_d}\n'
                 f'DAC_CAL_CTDC_TOA = {dac_cal_ctdc}')
    ToA_hist_ax = axes[0, 0]
    ToA_inl_ax = axes[0, 1]
    CTDC_hist_ax = axes[1, 0]
    CTDC_inl_ax = axes[1, 1]
    ToA_hist_ax.hist(FULL_BIN_EDGES[:-1],
                     bins=FULL_BIN_EDGES,
                     weights=hist,
                     alpha=1,
                     stacked=False,
                     fill=False,
                     histtype='step')
    ToA_hist_ax.set_ylim((0, 120))
    ToA_hist_ax.set_xlabel('ToA Code')
    ToA_hist_ax.set_ylabel('Occurrence')
    ToA_hist_ax.set_title(
        f'ToA-TDC code distribution for channel {ch} of chip {cp}, half {h}')

    ToA_inl_ax.plot(FULL_BIN_EDGES[:-1] + 0.5, full_inl)
    ToA_inl_ax.set_title('Integral non linearity of the ToA')
    ToA_inl_ax.set_xlabel('ToA Code')
    ToA_inl_ax.set_ylabel('INL [LSB]')
    ToA_inl_ax.grid()

    CTDC_hist_ax.hist(CTDC_BINS_EDGES[:-1],
                      bins=CTDC_BINS_EDGES,
                      weights=ctdc_hist,
                      alpha=1,
                      stacked=False,
                      fill=False,
                      histtype='step')
    CTDC_hist_ax.set_ylim((0, 300))
    CTDC_hist_ax.axhline(
        np.mean(ctdc_hist),
        label='Mean of all CTDC codes',
        color='orange')
    CTDC_hist_ax.hlines(
        part_ctdc_mean,
        CTDC_BINS_EDGES[20],
        CTDC_BINS_EDGES[-20],
        label='mean of inner CTDC codes',
        color='red')
    CTDC_hist_ax.set_ylabel('Count')
    CTDC_hist_ax.set_xlabel('ToA-CTDC code')
    CTDC_hist_ax.grid()
    CTDC_hist_ax.set_title('ToA CTDC code distribution')
    CTDC_hist_ax.legend()
    CTDC_inl_ax.plot(CTDC_BINS_EDGES[:-1] + 0.5,
                     np.array(ctdc_inl) * 256 / sum(ctdc_hist))
    CTDC_inl_ax.set_title('Integral non linearity of the CTDC')
    CTDC_inl_ax.set_ylabel('INL [LSB]')
    CTDC_inl_ax.set_xlabel('CTDC Code')
    CTDC_inl_ax.text(0, -.8,
                     f"Inl Slope = {ctdc_slope_diff}:3")
    CTDC_inl_ax.plot(
        CTDC_BINS_EDGES[:-1] + 0.5,
        ((CTDC_BINS_EDGES[:-1] + 0.5) * ctdc_slope_diff +
         max(ctdc_inl)) * 256 / sum(ctdc_hist))
    return fig


def make_ftdc_structure_plot(tdc_summary_data: dict) -> figure:
    full_hist = tdc_summary_data['full_hist']
    ctdc_codes = np.arange(32)
    ftdc_codes = np.arange(8)
    ctdc_code_bins = np.arange(33) - .5

    FTDC_hists_per_ctdc_code = [select_ftdc_dist_from_ctdc_bin(
        full_hist, code, density=True) for code in ctdc_codes]
    kol_smir_dist = np.zeros(shape=(32, 32))
    for i in range(32):
        for j in range(32):
            kol_smir_dist[i][j] = sum(
                np.abs(FTDC_hists_per_ctdc_code[i] - FTDC_hists_per_ctdc_code[j]))

    # Produce the plot
    fig, ax = plt.subplots(1, 3, figsize=(
        24, 8), gridspec_kw=dict(width_ratios=[1.5, 1, 1]))
    im = ax[0].imshow(kol_smir_dist)
    ax[0].set_title('Kolmogorov-Smirnov distance of the FTDC-occupancy')
    ax[0].set_xlabel('CTDC code corresponding to FTDC-histogram')
    ax[0].set_ylabel('CTDC code corresponding to FTDC-histogram')
    fig.colorbar(im, ax=ax[0])
    # TODO plot CTDC code occupancy
    ctdc_hist = sum([np.array(full_hist[i*256:(i+1)*256])
                    for i in range(1024//256)])
    ctdc_code_occupancy = [sum(ctdc_hist[i*8:(i+1)*8])
                           for i in range(len(ctdc_hist)//8)]
    ax[1].hist(ctdc_code_bins[:-1], bins=ctdc_code_bins,
               weights=ctdc_code_occupancy, fill=False,  histtype='step')
    ax[1].set_xlabel('CTDC-code')
    ax[1].set_ylabel('occupancy')
    ax[1].set_title('CTDC-occupancy')
    ax[1].text(
        15,
        .2 * max(ctdc_code_occupancy),
        f"Chip: {tdc_summary_data['chip']}, Half: {tdc_summary_data['half']}, "
        f"Channel: {tdc_summary_data['ch']}\nCTRL_IN_SIG_CTDC: {tdc_summary_data['sig']}"
        f"\nCTRL_IN_REF_CTDC: {tdc_summary_data['ref']}\nDAC_CAL_CTDC_TOA: {tdc_summary_data['dac_cal_ctdc']}"
        f"\nCTRL_IN_SIG_FTDC: {tdc_summary_data['fsig']}\nCTRL_IN_REF_FTDC: {tdc_summary_data['fref']}"
        f"\nDAC_CAL_FTDC_TOA: {tdc_summary_data['dac_cal_ftdc']}",
        ha="center", va="center", size=11,
        bbox=dict(boxstyle="round,pad=0.5", fc="white", ec="b", lw=1))
    ftdc_bin_counts = np.array([full_hist[i::8] for i in range(8)]).transpose()
    ax[2].violinplot(ftdc_bin_counts, ftdc_codes, vert=True)
    ax[2].set_xlabel('FTDC-code')
    ax[2].set_ylabel(
        'Distribution of counts in ToA values\ncorresponding to FTDC-code $i$')
    ax[2].set_title(f"Distribution of ToA-code counts into the FTDC codes")
    return fig


def make_relative_ctdc_occupancy_plot(tdc_summary_data: dict) -> figure:
    # define the plot lables
    chip = tdc_summary_data['chip']
    half = tdc_summary_data['half']
    channel = tdc_summary_data['ch']
    ref = tdc_summary_data['ref']
    sig = tdc_summary_data['sig']
    ctdc_dac = tdc_summary_data['dac_cal_ctdc']
    ctdc_sections = prepare_data_for_ctdc_comparison(tdc_summary_data, 2**3)
    labels = ['section 1 / section 0',
              'section 2 / section 0', 'section 3 / section 0']
    pltcols = ['cyan', 'blue', 'blueviolet']
    # prepare the data for plotting
    d0 = ctdc_sections[1] / ctdc_sections[0]
    d1 = ctdc_sections[2] / ctdc_sections[0]
    d2 = ctdc_sections[3] / ctdc_sections[0]
    plt_data = np.array([d0, d1, d2]).transpose()
    # calculate the histogram bin edges
    min_data = min(map(lambda x: min(x), plt_data))
    max_data = max(map(lambda x: max(x), plt_data))
    lower_bin_bound = np.round(min_data, 1) if np.round(
        min_data, 1) < min_data else min_data - 0.1
    upper_bin_bound = np.round(max_data, 1) if np.round(
        max_data, 1) > max_data else max_data + 0.1
    bins = np.linspace(lower_bin_bound, upper_bin_bound, 15, endpoint=True)
    # plot the histogram

    fig, ax = plt.subplots(1, 2, figsize=(
        8, 5), sharex="col", gridspec_kw=dict(width_ratios=[3, 1]))
    ax[1].hist(plt_data, bins=bins,
               alpha=1,
               label=labels,
               color=pltcols,
               stacked=False,
               fill=False,
               histtype='step',
               orientation="horizontal",
               linewidth=2)
    ax[1].set_ylim(lower_bin_bound, upper_bin_bound)
    ctdc_codes = -.5 + np.arange(32)
    # plot the ratios ctdc-code for ctdc-code
    for dat, col, label in zip(plt_data.transpose(), pltcols, labels):
        ax[0].step(ctdc_codes, dat, color=col, label=label)
        ax[0].set_ylim(lower_bin_bound, upper_bin_bound)
    ax[0].legend()
    ax[0].set_xlabel('CTDC-code')
    ax[0].set_ylabel('relative occupancy')
    ax[0].set_title(
        f'Chip={chip}, Half={half}, Chan={channel}\nCTRL_IN_REF_CTDC={ref},  '
        f'CTRL_IN_SIG_CTDC={sig}\nDAC_CAL_CTDC_TOA={ctdc_dac}\n\nrelative ToA Code count')
    return fig


def plot_ctdc_inl(plot_dir: Path, grad: dict, ensure_channel_dir=True) -> None:
    cp = grad['chip']
    h = grad['half']
    ch = grad['ch']
    dac_cal_ctdc = grad['dac_cal_ctdc']
    sig = grad['sig']
    ref = grad['ref']
    if ensure_channel_dir:
        chan_dir = ensure_channel_output_dir(plot_dir, 'ctdc', grad)
    else:
        chan_dir = plot_dir
    fig = make_ctdc_inl_gradient_plot(grad)
    fig.savefig(chan_dir / f'toa-master-ctdc-calib-chip-{cp}-half-{h}-channel-{ch}-sig-{sig}'
                f'-ref-{ref}-dac-tdc-{dac_cal_ctdc}.pdf')
    plt.close(fig)


def plot_ftdc_inl(plot_dir: Path, grad: dict, ensure_channel_dir=True) -> None:
    cp = grad['chip']
    h = grad['half']
    ch = grad['ch']
    dac_cal_ftdc = grad['dac_cal_ftdc']
    fsig = grad['fsig']
    fref = grad['fref']
    if ensure_channel_dir:
        chan_dir = ensure_channel_output_dir(plot_dir, 'ftdc', grad)
    else:
        chan_dir = plot_dir
    fig = make_ftdc_inl_gradient_plot(grad)
    fig.savefig(chan_dir / f'toa-master-ftdc-calib-chip-{cp}-half-{h}-channel-{ch}-fsig-{fsig}'
                f'-fref-{fref}-fdac-{dac_cal_ftdc}.pdf')
    plt.close(fig)


def plot_relative_ctdc_occupancy(output_dir: Path, tdc_summary_data: dict, ensure_channel_dir=True) -> None:
    sig = tdc_summary_data['sig']
    ref = tdc_summary_data['ref']
    ctdc_dac = tdc_summary_data['dac_cal_ctdc']
    channel = tdc_summary_data['ch']
    chip = tdc_summary_data['chip']
    half = tdc_summary_data['half']
    fig = make_relative_ctdc_occupancy_plot(tdc_summary_data)
    if ensure_channel_dir:
        plot_subdir = ensure_channel_output_dir(
            output_dir, 'ctdc', tdc_summary_data)
    else:
        plot_subdir = output_dir
    fig.savefig(
        plot_subdir / f'ctdc-relative-code-density-chip-{chip}-half-{half}-ch-{channel}-sig-{sig}-ref-{ref}-dac-{ctdc_dac}.pdf')
    plt.close(fig)


def plot_ftdc_correations(output_dir: Path, tdc_summary_data: dict, ensure_channel_dir=True) -> None:
    fsig = tdc_summary_data['fsig']
    fref = tdc_summary_data['fref']
    ftdc_dac = tdc_summary_data['dac_cal_ftdc']
    channel = tdc_summary_data['ch']
    chip = tdc_summary_data['chip']
    half = tdc_summary_data['half']
    fig = make_ftdc_structure_plot(tdc_summary_data)
    if ensure_channel_dir:
        plot_subdir = ensure_channel_output_dir(
            output_dir, 'ftdc', tdc_summary_data)
    else:
        plot_subdir = output_dir
    fig.savefig(
        plot_subdir / f'ftdc-code-density-correlation-chip-{chip}-half-{half}-ch-{channel}-fsig-{fsig}-fref-{fref}-fdac-{ftdc_dac}.pdf')
    plt.close(fig)
