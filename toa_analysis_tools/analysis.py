"""
Analysis to plot and measure the characteristics of the TOA
"""
from pathlib import Path
import time
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib import gridspec


class toaAnalysis():
    def __init__(self, analysis_parameters):
        """Initialize the analyses with the parameters from
        the configuration. (The configuration is parsed and
        processed by the Datenraffinerie, so here we get a
        Dictionary with the parameters of the corresponding
        configuration entry.

        :analysis_parameters: A dictionary containing the parameters
        from the analysis configuration

        """
        self._analysis_parameters = analysis_parameters

    def output(self):
        """ Declare the files that constitute the output of the analysis

        :returns: dict containing three entries, The 'summary' entry is a
        path to a summary file, the 'plots' entry is a list of paths (str) to
        the plots produced by the analysis and the 'calibration' entry is a path
        to a calibration configuration that can be used to update existing
        configurations with the calibration obtained from the data.
        A calibration is not mandatory (nor are any of the other files) except
        for analyses that are used explicitly as calibration analyses.
        """

        return {
            'summary': None,
            'plots': ['InjectionvsTOA.pdf', 'TOA_and_phase_scan.pdf'],
            'calibration': None
        }

    def run(self, data: pd.DataFrame, output_dir: Path):
        """ Perform the analysis in this function. don't forget to create the all
        the files and directories in the return value of the `output` function
        the parameters passed into the object during creation can be used to determin
        various things during the analysis but are not used here

        the code in this function is an example for how an analysis could look like
        without actually being an analysis of any kind

        :data: The pandas Dataframe containing all the chip parameters and measurement
            results of the entire daq process the format of the Dataframe will be described
            in the README/Wiki of the Datenraffinerie
        :output_dir: the directory that the output plots and summaries should be placed
            into. the outputs of the analysis should be exclusively places in this folder
            as not to upset any of the other analyses or acquisition processes.
        """
        output_dir = Path(output_dir)
        active_channels = set(data.loc[data['HighRange'] == 1]['channel'])
        chips = set(data['chip'])
        fig = plt.figure(figsize=(6*len(chips), 5))
        chip_grids = gridspec.GridSpec(1, len(chips), figure=fig)
        for chip, grid in zip(chips, chip_grids):
            toa_plot_grid = gridspec.GridSpecFromSubplotSpec(3, 3, subplot_spec=grid)
            chip_data = data.loc[data['chip'] == chip]
            plotax = fig.add_subplot(toa_plot_grid[:-1, :])
            panelax = fig.add_subplot(toa_plot_grid[-1, :])
            for channel in active_channels:
                channel_data = chip_data.loc[data['channel'] == channel]
                plotax.plot(channel_data['Calib'], channel_data['toa_mean'], label=f'Channel: {channel}')
                panelax.plot(channel_data['Calib'], channel_data['toa_mean']/channel_data['toa_median'])
            plotax.legend()
            panelax.set_xlabel('Injection DAC Setting')
            panelax.set_ylabel(r'TOA mean \ TOA median')
            plotax.set_ylabel('Mean TOA')
            plotax.set_title(f'CalibDAC vs TOA, Chip={chip}')
        fig.savefig(output_dir / self.output()['plots'][0])
        channels = set(data.loc[data['HighRange'] == 1]['channel'])
        chips = set(data['chip'])
        phases = list(set(data['phase_strobe']))[::2]
        fig = plt.figure(figsize=(6*len(channels), 8*len(chips)))
        chip_channel_grid = gridspec.GridSpec(len(channels), len(chips), figure=fig)
        for i, chip in enumerate(chips):
            chip_data = data.loc[data['chip'] == chip]
            for j, channel in enumerate(channels):
                plot_grid = gridspec.GridSpecFromSubplotSpec(3, 3, subplot_spec=chip_channel_grid[j*len(chips)+i])
                plotax = fig.add_subplot(plot_grid[:-1, :])
                plotax.set_title(f'TOA vs Injection, Chip={chip} Channel={channel}')
                plotax.set_ylabel('TOA count')
                panelax = fig.add_subplot(plot_grid[-1, :])
                panelax.set_xlabel('CalibDAC count')
                panelax.set_ylabel('mean/median')
                channel_data = chip_data.loc[chip_data['channel'] == channel]
                for phase in phases:
                    phase_strobe_data = channel_data.loc[channel_data['phase_strobe'] == phase]
                    plotax.plot(phase_strobe_data['Calib'], phase_strobe_data['toa_mean'], 'x', label=f'phase = {phase}')
                    panelax.plot(phase_strobe_data['Calib'], phase_strobe_data['toa_mean']/phase_strobe_data['toa_median'])
                plotax.legend()
        fig.savefig(output_dir / self.output()['plots'][1])
