import logging
from pathlib import Path
import click
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.gridspec import GridSpec
import numpy as np


mpl.use('qtagg')
# dictionary that converts between string and debug level
log_level_dict = {'DEBUG': logging.DEBUG,
                  'INFO': logging.INFO,
                  'WARNING': logging.WARNING,
                  'ERROR': logging.ERROR,
                  'CRITICAL': logging.CRITICAL}


@click.command(help='Tool to plot the code density for different ACG settings')
@click.argument('data-file', type=click.Path(exists=True, file_okay=True,
                                             dir_okay=False, readable=True))
@click.option('-n', '--plot-name', type=str, default='acg-hist.pdf',
              help='Name of the file that the plot should be saved as')
@click.option('-c', '--channel', type=int, multiple=True, default=None,
              help='Channels to show in the plot')
@click.option('-s', '--show', is_flag=True, default=False, show_default=True,
              help='Flag that shows the plot on the screen when set')
@click.option('-w', '--width', type=float, default=2,
              help='width of the a plot in the figure in inches')
@click.option('-h', '--height', type=float, default=2,
              help='height of the a plot in the figure in inches')
@click.option("-l", "--log", type=click.Path(),
              default='plot-acg-rates.log',
              help="file to write the logs to")
@click.option('-v', "--loglevel", type=click.Choice(log_level_dict.keys()),
              default="INFO", show_default=True,
              help="Set the loglevel")
def acg_tdc_hist(data_file, plot_name, channel,
                 show, width, height, log, loglevel):
    """
    Produce plots showing the hits for different ACG gains and VCO voltages
    """
    # set up logging
    logging.basicConfig(filename=log, level=log_level_dict[loglevel],
                        filemode='w+',
                        format='[%(asctime)s] %(levelname)-10s:'
                               '%(name)-30s %(message)s')
    logger = logging.getLogger('plot-data-processing')

    # rename vairable name
    plot_channels = channel

    # create the figure
    frame = pd.read_hdf(data_file, 'table')
    frame = frame.unstack(level=[-1, -2, -3])
    # extract relevant data from input data
    channels = frame.columns.drop_duplicates()
    acg_gains = frame.index.get_level_values(0).drop_duplicates()
    vco_vals = frame.index.get_level_values(1).drop_duplicates()
    if len(channel) == 0:
        plot_channels = [ch[-1] for ch in channels]

    # create the plots for the channel
    fig, axes = plt.subplots(len(plot_channels), len(acg_gains))
    figheight = len(plot_channels) * height
    fig.set(figheight=figheight)
    fig.set(figwidth=width * len(acg_gains))
    fig.subplots_adjust(bottom=0.1)
    cbar_ax = fig.add_axes([0.12, 0.04, 0.78, 0.02])

    # cmap = plt.cm.viridis
    cmap = mpl.cm.coolwarm_r
    norm = mpl.colors.Normalize(vmin=min(vco_vals), vmax=max(vco_vals))
    # we look at the density averages for the different gains
    for i, channel in enumerate(channels):
        if channel[-1] not in plot_channels:
            continue
        logger.info('Plotting data for channel %d' % channel[-1])
        for j, gain in enumerate(acg_gains):
            logger.info('Plotting data for acg_gain = %d' % gain)
            vco_vals = list(frame.loc[pd.IndexSlice[gain, :, :], channel].index.get_level_values(
                1).drop_duplicates())
            vco_vals.reverse()
            sub_frames = [frame.loc[pd.IndexSlice[gain, val, :], channel].values
                          for val in vco_vals]
            axis = axes[i][j]
            axis.set_title(
                f'Code densities for channel {channel[-1]}, acg_gain {gain}')
            axis.set_xlabel('TDC Code')
            axis.set_ylabel('Code occupancy')
            if i > 0:
                axis.sharex(axes[0][j])
            hist, _, _ = axis.hist(
                sub_frames,
                bins=np.linspace(
                    0, 1024, 65, endpoint=True),
                histtype='step',
                fill=False,
                stacked=False,
                color=[cmap(val/max(vco_vals))
                       for val in vco_vals],
                label=[f"INIT_D = {val}" for val in vco_vals])
            if j == 2:
                axis.set_ylim((0, 6000))
            else:
                axis.set_ylim((0, np.mean(hist) + 2 * np.std(hist)))
    fig.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap),
                 cax=cbar_ax, orientation='horizontal', label='INIT_D')
    fig.savefig(plot_name)
    if show:
        plt.show()
    plt.close()


if __name__ == "__main__":
    acg_tdc_hist()
