import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors
import click
import pandas as pd


def plot_channel_sig_ref_hist(data):
    channel = data.columns[0]
    cmap = plt.cm.viridis
    norm = colors.Normalize(vmin=-32, vmax=32)
    sigs = [31, 16, 0, 0, 0]
    refs = [0, 0, 0, 16, 31]
    fig, axes = plt.subplots(5, 1, figsize=(10, 10), layout='constrained')
    for sig, ref, ax in zip(sigs, refs, axes[::-1]):
        data_slice = []
        for name in data.index.names:
            if name == 'CTRL_IN_SIG_CTDC_P_D':
                data_slice.append(sig)
            elif name == 'CTRL_IN_REF_CTDC_P_D':
                data_slice.append(ref)
            else:
                data_slice.append(slice(None))
        subframe = data.loc[tuple(data_slice), channel]
        ax.hist(subframe.values,
                bins=np.arange(1024) - 0.5,
                label=f"SIG = {sig}, REF = {ref}",
                color=cmap(norm(sig - ref)))
        # ax.set_xlabel('TDC code')
        ax.sharex(axes[0])
        ax.set_ylabel('Code occupancy')
        ax.legend()
        ax.set_ylim(0, 130)
    axes[0].set_title(
        'TDC-code occupancy')
    axes[-1].set_xlabel('TDC Code')
    fig.savefig("measured-toa-hist-ref-scan.pdf")
    plt.close(fig)


@click.command()
@click.argument('data-file', type=click.Path(exists=True, dir_okay=False))
def channel_sig_ref_hist(data_file):
    """
        Plot the histogram similar to the simulation for acquired data
    """
    data = pd.read_hdf(data_file)
    data = data.unstack(level=[-1, -2, -3])
    plot_channel_sig_ref_hist(data)


if __name__ == "__main__":
    channel_sig_ref_hist()
