"""
Author: Alexander Becker (KIT) <a.becker@cern.ch>
Date: 30.12.2022


Low CTDC Count Bin occupancy plot
=================================

This plot explores how the occupancy of the codes of low ctdc values behave for
different switching speeds of the master DLL, which means for different
settings of the CTRL_IN_SIG_CTDC (sig) and CTRL_IN_REF_CTDC (ref) parameters.

Expectation:
------------
Assuming that the sig parameter speeds up the master DLL and the REF parameter
slows down the master DLL the following should occurr.

A 'fast' master DLL (large sig) will cause the error generated
by the master DLL to set the channel DLLs to run slow. (vice versa for the ref
parameter).

A slow channel DLL will mean that the occupancy of the channels that can be
reached is higher than for an accurately tuned master DLL. Due to the
design of the ToA small ToA codes will not be reached if the buffers are
set to run to slow, even for very small inputs. For the range of ToA codes
of interest in this experiment, this means that for large sig values a very
low occupancy is expected, which rises roughly linearly for decreasing sig
values until falling again for growing ref values with a rise again for large
ref values.

Methodology
-----------
Using the ToA-simulation a sample of roughly 40_000 events is generated for
each sig and ref code. Then the data from a sig and a ref scan (lab test using
ACG) is used run through the same plot to evaluate the results form the actual
ToA

Observations
------------
* A count of the events containing ToA values > 0 reveals that there are about
  30k to 35k ToA triggers in 250k samples. As the ACGs are independent in every
  Chip Half, the data needs to be analysed half wise to suppress the likely 0s in
  the other halfs.
"""
from pathlib import Path
import logging
import matplotlib.pyplot as plt
import click
from extract_toa_data_from_drf_data import combine_data_from_sig_ref_scan
from extract_toa_data_from_drf_data import load_datenraffinerie_data
from extract_toa_data_from_drf_data import extract_acg_tuning_toa_data
from extract_toa_data_from_drf_data import write_half_wise_data_to_disk

# dictionary that con
log_level_dict = {'DEBUG': logging.DEBUG,
                  'INFO': logging.INFO,
                  'WARNING': logging.WARNING,
                  'ERROR': logging.ERROR,
                  'CRITICAL': logging.CRITICAL}


@click.command('preprocess-data-for-toa-analysis',
               help='Preprocess the data for the ToA-calibration by pivoting '
               'it and sorting the data by half-chip. The resulting file '
               'contains the information on the ToA for the entire scan')
@click.argument("sig-folder", type=click.Path(exists=True, dir_okay=True))
@click.argument("ref-folder", type=click.Path(exists=True, dir_okay=True))
@click.option("-g", "--group-name", type=str, default='data',
              help="The name of the group that the aggregated "
                   "ToA data is put into")
@click.option("-l", "--log", type=click.Path(),
              default="toa-preprocessing.log",
              help="file to write the logs to")
@click.option('-v', "--loglevel", type=click.Choice(log_level_dict.keys()),
              default="INFO", show_default=True,
              help="Set the loglevel")
def preprocess_data_for_toa_analysis(sig_folder, ref_folder, group_name,
                                     log, loglevel):
    logging.basicConfig(filename=log, level=log_level_dict[loglevel],
                        filemode='w+',
                        format='[%(asctime)s] %(levelname)-10s:'
                               '%(name)-30s %(message)s')
    combine_data_from_sig_ref_scan(
        Path(sig_folder), Path(ref_folder), name=group_name)


@click.command(
    'preprocess-data-acg-event-rate-tuning',
    short_help='Preprocess the Data from scanning the ACG settings.',
    help='Reshape the data to simplify finding the optimal ACG'
    ' settings.\nColumns=["chip", "half", "channel"],'
    '\nindex=["INIT_D", "rcg_gain", "event"],\nvalues="toa"')
@click.argument("data-folder", type=click.Path(exists=True, dir_okay=True))
@click.argument("output-folder", type=click.Path(dir_okay=True))
@click.option("-l", "--log", type=click.Path(),
              default="toa-event-rate-tuning-preprocessing.log",
              help="file to write the logs to")
@click.option('-v', "--loglevel", type=click.Choice(log_level_dict.keys()),
              default="INFO", show_default=True,
              help="Set the loglevel")
def preprocess_data_acg_event_rate_tuning(data_folder, output_folder,
                                          log, loglevel):
    """
    function to run when wanting to preprocess the data for acg-rate-tuning
    """
    logging.basicConfig(filename=log, level=log_level_dict[loglevel],
                        filemode='w+',
                        format='[%(asctime)s] %(levelname)-10s:'
                               '%(name)-30s %(message)s')
    logger = logging.getLogger('main')
    output_folder = Path(output_folder)
    if not output_folder.exists():
        logger.debug('creating output folder %s' % output_folder.absolute())
        output_folder.mkdir()
    data_folder = Path(data_folder)
    data_files = list(data_folder.glob('run_*_data.h5'))
    logger.info('Found %d files in %s' %
                (len(data_files), data_folder.absolute()))
    for file in data_files:
        logger.info('Processing data in file %s' % str(file))
        data = load_datenraffinerie_data(file)
        logger.info('Data loaded')
        hw_data = extract_acg_tuning_toa_data(data)
        write_half_wise_data_to_disk(hw_data, output_folder,
                                     file_prefix='acg-event-rate-tuning-data')


@click.command("low-toa-code-occupancy")
@click.argument("data-folder", type=click.Path(exists=True, dir_okay=True))
@click.option("-l", "--log", type=click.Path(),
              default="low_ctdc_count_occupancy_analysis.log",
              help="file to write the logs to")
@click.option('-v', "--loglevel", type=click.Choice(log_level_dict.keys()),
              default="INFO", show_default=True,
              help="Set the loglevel")
@click.option("-e", "--channel-event-count", is_flag=True, default=False,
              help="perform a count of all ToA events in a channel for every"
                   "channel in a data file")
@click.option("-n", "--noise-floor", type=int, default=1000,
              help="Set the amount of events that have to be present in a"
                   "channel to be considered 'enabled' and used in the analysis")
def cli(data_folder, log, loglevel, channel_event_count, noise_floor):
    """
    Provide the command line interface for running the Analyses
    """
    # constants to control program flow
    chan_event_count = False

    logging.basicConfig(filename=log, level=log_level_dict[loglevel],
                        filemode='w+',
                        format='[%(asctime)s] %(levelname)-10s:'
                               '%(name)-30s %(message)s')
    logger = logging.getLogger("main")
    data_folder = Path(data_folder)
    data_files = sorted(data_folder.glob("*.h5"))

    # There are about 30 files for the ref scan and about
    # 30 files for the sig scan that need to be read in
    for filename in data_files:
        logger.debug("found data file %s" % filename)
        logger.debug("loading data from file %s" % filename)
        data = load_datenraffinerie_data(filename)
        logger.debug("data contains %d events" %
                     len(data.event.drop_duplicates()))
        events = get_events_with_toa_value(data)
        logger.info("found a total of %d events with ToA values > 0" %
                    len(events))

        # plot the overview of channels in this dataset
        data['chan_id'] = (data.chip * 2 + data.half) * 39 + data.channel
        if chan_event_count:
            fig, ax = plt.subplots()
            logger.info(
                "Counting the events that containd data filtered by channel")
            chan_ids = data.chan_id.drop_duplicates()
            chan_counts = [len(data.loc[(data.chan_id == chan_id) & (
                data.toa > 0)].event.drop_duplicates()) for chan_id in chan_ids]
            ax.bar(chan_ids, chan_counts)
            ax.set_xlabel("Channel ID")
            ax.set_ylabel("Events with triggered ToA")
            ax.set_title(
                f"Events of interest sorted by channel_id in {filename}")
            plt.show()
            plt.close(fig)

        # split the data into data from a single half, as there is an ACG
        # per half and so the data in two different halfs will be uncorrelated
        hw_datasets = split_data_into_halfs_and_filter_out_events(data)

        # we are interested in how far the toa values of the different channels
        # are appart from each other, assuming that the ACG triggered all the
        # channels at the same time. Then we want to see how this changes for
        # the different settings of the CTDC trim values (expectation is,
        # that it should not
        for chip, half, half_wise_dataset in hw_datasets:

            # the first thing that we want to do here is to look at the spred
            # of the ToA channels of the same half that are enabled.
            # The enabled channels are the ones that have a lot of event data
            hw_chan_ids = half_wise_dataset.chan_id.drop_duplicates()
            channels_with_data = list(filter(lambda x: len(half_wise_dataset.loc[(
                half_wise_dataset.chan_id == x) & (half_wise_dataset.toa > 0)]) > noise_floor, hw_chan_ids))
            half_wise_dataset = half_wise_dataset.loc[half_wise_dataset.chan_id.isin(
                channels_with_data)]
            hw_toa_data = half_wise_dataset.pivot(
                index='event', columns='chan_id', values='toa')


if __name__ == "__main__":
    preprocess_data_acg_event_rate_tuning()
