"""
Author: Alexander Becker
Date: 02.01.2023

Extract, clean and aggregate ToA data from the measurements and for
the ToA calibration procedures.

This module contains functions that extract the ToA data from
multiple runs, removes unneeded data and aggregates this data into a
single DataFrame.
"""
import logging
from pathlib import Path
from copy import deepcopy, copy
import pandas as pd
import tables


def level_from_name(index: pd.MultiIndex, name: str):
    """
    Find the level of the subindex with the name "name"
    """
    for i, i_name in enumerate(index.names):
        if name == i_name:
            return i
    return None


def get_subframes_for_index_values(frame: pd.DataFrame, index_column_names: list[str]):
    """
    Get the a list of slices for every valid value combination found in the dataframe
    for all columns specified in index_column_names.
    """
    index_column_indicies = []
    for icn in index_column_names:
        for i, name in enumerate(frame.index.names):
            if icn == name:
                index_column_indicies.append(i)
                break
    indices_not_in_selection = []
    for i, name in enumerate(frame.index.names):
        if name not in index_column_names:
            indices_not_in_selection.append(i)
    values_of_selected_index_columns = frame.index.droplevel(
        indices_not_in_selection).drop_duplicates()
    index_selection_slices = []
    selection_slice_template = [slice(None) for _ in frame.index.names]
    for value in values_of_selected_index_columns:
        sel_slice = deepcopy(selection_slice_template)
        for i, val in zip(index_column_indicies, value):
            sel_slice[i] = val
        index_selection_slices.append(tuple(sel_slice))
    return index_column_indicies, index_selection_slices


def load_datenraffinerie_data(filename: str) -> pd.DataFrame:
    """
    Function to load data from the datenraffinerie
    """
    data_tb = tables.open_file(filename)
    data = pd.DataFrame.from_records(data_tb.root.data.measurements.read())
    data_tb.close()
    return data


def split_data_into_half_wise_datasets(frame: pd.DataFrame) -> list[tuple[int, int, pd.DataFrame]]:
    """
    When using the ACG, the ACGs in the different chip halves are not
    synchronized wich is why the triggers from the ACG arrive at different
    times in the different halfs. This means that the ACG calibration can
    only be done by looking at the data in 'half wise' chunks.

    This function chunks the data into data coming from only one of the
    halfs found in the data. This works on the unpivoted data and may be
    slower as a result as more is executed in python
    """
    c_h = frame.loc[:, ['chip', 'half']].drop_duplicates()
    hw_datasets = []
    for _, (chip, half) in c_h.iterrows():
        hw_datasets.append((chip, half, frame.loc[
            (frame['chip'] == chip) & (frame['half'] == half)]))
    return hw_datasets


def extract_events_with_toa_from_raw_data(frame: pd.DataFrame) -> pd.DataFrame:
    """
    As the ACG does not fire with the same 40MhZ frequency, only some events
    will contain ToA data that is not 0. Assuming that the data in `frame` is
    data coming only from one chip half, the events only containing 0 as ToA
    value are dropped (at the highest working ToA setting this is 4/25 of
    all events)
    """
    events_with_data = frame.loc[frame.toa > 0].event.drop_duplicates()
    return frame.loc[frame.event.isin(events_with_data)]


def extract_events_with_toa_from_pv_data(hw_frame: pd.DataFrame) -> pd.DataFrame:
    """
    As the ACG does not fire with the same 40MhZ frequency, only some events
    will contain ToA data that is not 0. Assuming that the data in `frame` is
    data coming only from one chip half, the events only containing 0 as ToA
    value are dropped (at the highest working ToA setting this is 4/25 of
    all events)

    Assume data 'pivoted' such that the index is the set of control variables
    (like REF, SIG, CTDC_CAL_DAC) along with the events recorded and the
    columns are the channels (of the half we are interested in) while the
    value in the coressponding filed is the ToA valued measured for this
    particular event
    """
    events_with_data = hw_frame.loc[hw_frame.sum(
        axis=1) > 0].event.drop_duplicates()
    return events_with_data


def extract_enabled_channels_from_pv_data(hw_frame: pd.DataFrame,
                                          toa_event_threshold: int) -> list[int]:
    """
    Extract the enabled channels by counting how many ToA values where found
    that are larger than 0 and comparing it to the threshold set by the
    `toa_event_threshold`
    """
    return list(filter(
        lambda c: hw_frame[c][hw_frame[c] > 0].count() >= toa_event_threshold,
        hw_frame.columns.get_level_values(-1)))


def pivot_data_to_toa(frame: pd.DataFrame) -> pd.DataFrame:
    """
    During the calibration with the ACG only some channels are enabled and
    connected to the ACG output. This is done to reduce the loading on the
    ACG output.

    This means that the data in all channels that have not been connected to
    the ACG will (nearly) always be 0, these channels can therefore be dropped
    from the frame.
    """
    pv_frame = frame.pivot(index=['CTRL_IN_REF_CTDC_P_D',
                                  'CTRL_IN_SIG_CTDC_P_D',
                                  'DAC_CAL_CTDC_TOA', 'event'],
                           columns=['chip', 'half', 'channel'], values='toa')
    return pv_frame


def extract_acg_tuning_toa_data(frame: pd.DataFrame) -> list[tuple[int, int, pd.DataFrame]]:
    """
    extract the data relevant to ACG tuning from the data-frame passed to the function
    and append that data to
    """
    logger = logging.getLogger('acg-data-extraction')
    logger.debug('filtering out only events that have a toa entry')
    events_with_toa = frame[frame.toa > 0].event.drop_duplicates()
    frame = frame[frame.event.isin(events_with_toa)]
    logger.info('pivoting data')
    frame = frame.pivot(
        index=['rcg_gain', 'INIT_D', 'event'],
        columns=['chip', 'half', 'channel'], values='toa')
    logger.info('extracting available chip halves from data')
    chip_halves = frame.columns.drop_level('channel').drop_duplicates()
    logger.debug(
        'availabe chip halves: [(chip, half), ...] %s' % str(chip_halves))
    data = []
    for chip, half in chip_halves:
        logger.info('extracting data for chip: %d, half: %d' % (chip, half))

        # select only the data belonging to one chip half
        hw_frame = frame.loc(axis=1)[pd.IndexSlice[chip, half, :]]
        total_events = len(hw_frame)

        # now we are only interested in the events where at least one
        # channel shows a ToA of > 0
        events_with_data = hw_frame.sum(axis=1) > 0
        channels_with_data = hw_frame.sum() > 0
        hw_frame = hw_frame.loc[events_with_data, channels_with_data]
        logger.debug("Events with data %d out of %d" %
                     (len(hw_frame), total_events))
        logger.debug("Channels with data: %s" %
                     str(hw_frame.columns.get_level_values(-1)))
        data.append((chip, half, hw_frame))
    return data


def extract_toa_data(frame: pd.DataFrame) -> list[tuple[int, int, pd.DataFrame]]:
    """
    Extract Data from the pandas DataFrame that was created by the
    Datenraffinerie
    """
    # bring the data into a shape that makes it easier to work with
    logger = logging.getLogger('toa-data-extraction')
    logger.info("Restructuring data (pivot)")
    frame = pivot_data_to_toa(frame)

    # extract all the indices for which the data is interesting
    logger.info("finding available chip-halves")
    chip_halves = frame.columns.drop_level('channel').drop_duplicates()
    cleaned_data = []
    for chip, half in chip_halves:
        logger.info("Extracting data for chip %d, half %d" % (chip, half))
        hw_frame = frame[chip, half]
        chip_settings = frame.index.droplevel(-1).drop_duplicates()
        try:
            assert len(chip_settings) == 1
            chip_settings = list(chip_settings[0])
            hw_events_with_data = extract_events_with_toa_from_pv_data(
                hw_frame)
            full_event_indices = []
            for event in hw_events_with_data:
                lcs = copy(chip_settings)
                lcs.append(event)
                full_event_indices.append(tuple(lcs))
            logger.info(
                "Extracting enabled channels for chip %d, half %d" % (chip, half))
            channels_with_data = [(chip, half, chan)
                                  for chan in extract_enabled_channels_from_pv_data(hw_frame, 2500)]
            cleaned_data.append(
                (chip, half, frame.loc[full_event_indices, channels_with_data]))
        except AssertionError as err:
            logger.info("Detected new Mk2 toa data")
            dac_cal_level = list(map(lambda n: n[0], filter(
                lambda n: n[1] == 'DAC_CAL_CTDC_TOA', enumerate(chip_settings.names))))
            dac_cal_values = hw_frame.index.get_level_values(dac_cal_level[0])
            if 31 not in dac_cal_values:
                raise ValueError("The DAC_CAL_CTDC_TOA needs to be set to 31, "
                                 "no such setting could be found in the data") from err
            selection = [slice(None, None) for _ in range(dac_cal_level[0])]
            selection.append(31)
            logger.debug("Applying selection to hw_frame: %s" %
                         str(selection))
            col_clean_df = frame.loc[tuple(
                selection), (chip, half, slice(None, None))].dropna(axis=1)
            clean_df = col_clean_df.loc[col_clean_df.sum(axis=1) > 0]
            cleaned_data.append(
                (chip, half, clean_df))
    return cleaned_data
