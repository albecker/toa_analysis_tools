import logging
import numpy as np
import pandas as pd
import click
import matplotlib.pyplot as plt
import matplotlib as mpl
from extract_toa_data_from_drf_data import get_subframes_for_index_values
from matplotlib.gridspec import GridSpec


log_level_dict = {'DEBUG': logging.DEBUG,
                  'INFO': logging.INFO,
                  'WARNING': logging.WARNING,
                  'ERROR': logging.ERROR,
                  'CRITICAL': logging.CRITICAL}


@click.command()
@click.argument('data-file', type=click.Path(exists=True, dir_okay=False))
@click.argument("figure-name", type=click.Path(dir_okay=False))
@click.option("-l", "--log", type=click.Path(),
              default="optimal-ctdc-search-plot.log",
              help="file to write the logs to")
@click.option('-v', "--loglevel", type=click.Choice(log_level_dict.keys()),
              default="INFO", show_default=True,
              help="Set the loglevel")
@click.option('-cm', '--colormap', type=str, default='viridis',
              help='choose the colormap for the plot')
@click.option('-w', '--width', type=int, default=7,
              help='width of the figure', show_default=True)
@click.option('-h', '--height', type=int, default=4,
              help='height of the figure', show_default=True)
def plot_optimal_ctdc_search(data_file, figure_name, log, loglevel, colormap, width, height):
    """
    Plot the distance metric for every channel and every sig-ref setting
    for half-wise data
    """
    logger = logging.getLogger("ctdc_trim_error_plot")
    logging.basicConfig(filename=log, level=log_level_dict[loglevel],
                        filemode='w+',
                        format='[%(asctime)s] %(levelname)-10s:'
                               '%(name)-30s %(message)s')
    logger.info("Reading in data from %s" % data_file)
    data = pd.read_hdf(data_file)
    data = data.unstack(level=[-1, -2, -3])
    channel = data.columns
    cmap = mpl.colormaps[colormap]
    logger.info("partitioning data")
    column_of_name_in_index, index_slices = get_subframes_for_index_values(
        data, ['CTRL_IN_SIG_CTDC_P_D', 'CTRL_IN_REF_CTDC_P_D'])
    sections = [slice(0, 0x14, 1), slice(0x0ea, 0x114, 1), slice(
        0x1ea, 0x214, 1), slice(0x2ea, 0x314, 1), slice(0x3ea, 0x3ff, 1)]
    channel_nums = list(map(lambda c: c[-1], channel))
    norm = mpl.colors.Normalize(vmin=min(channel_nums), vmax=max(channel_nums))

    fig = plt.figure(figsize=(width, height))
    gs = GridSpec(2, 1, height_ratios=[2, 1])
    ax1 = fig.add_subplot(gs[0])
    ax2 = fig.add_subplot(gs[1])
    error_data = []
    for chan in channel:
        logger.info('Analysing data for channel %s' % str(chan))
        plt_data = []
        for idx_sl in index_slices:
            subframe = data.loc[idx_sl, chan]
            sig = idx_sl[column_of_name_in_index[0]]
            ref = idx_sl[column_of_name_in_index[1]]
            logger.info("Analysing data for SIG=%d, REF=%d" % (sig, ref))
            # we need to do this dance to make sure
            # that columns without entries appear in the
            # histogram
            tempframe = pd.Series(np.zeros(1024, dtype=int), name=chan)
            tempframe += subframe.value_counts()
            tempframe = tempframe.fillna(0).astype(int)
            channel_average = tempframe.mean()
            logger.info("Channel average for channel %s = %f" %
                        (chan, channel_average))
            section_means = [tempframe.loc[sec].mean() for sec in sections]
            combined_sections = pd.concat(
                [tempframe.loc[sec] for sec in sections])
            logger.info("Mean for sections = %s" % section_means)
            logger.info("Mean of sections combined = %.2f" %
                        combined_sections.mean())
            trim_error = combined_sections.mean() - channel_average
            plt_data.append(
                (sig, ref, trim_error))
            error_data.append((chan, sig - ref, trim_error))
        plt_x = list(map(lambda v: v[0] - v[1], plt_data))
        plt_y = list(map(lambda v: v[2], plt_data))
        ax1.scatter(plt_x, plt_y, color=cmap(
            norm(chan[-1])), label=f'Channel {chan[-1]}')
    ax1.legend()
    ax1.set_ylabel(r'$E_{Trim}$')
    ax1.set_title('Trim Error for different Reference TDC buffer speeds')

    # plot the magnitude of total error
    plt_x = sorted(set(map(lambda x: x[1], error_data)))
    plt_y = [sum(map(lambda x: x[-1], filter(lambda x: x[1]
                 == x_val, error_data))) for x_val in plt_x]
    abs_error = list(np.abs(plt_y))
    min_x = plt_x[abs_error.index(min(abs_error))]
    sig = min_x if min_x > 0 else 0
    ref = min_x if min_x < 0 else 0
    ax2.plot(plt_x, np.abs(plt_y), color='purple')
    ax2.set_ylabel(r"$\sum_{Channel} |E_{Trim}|$")
    ax2.set_xlabel("SIG - REF")
    ax2.axhline(min(abs_error), color='gray', linestyle='--')
    ax2.text(-30, min(abs_error),
             f"Optimal SIG={sig}, REF={ref}", ha="left", va="bottom")
    fig.savefig(figure_name)


if __name__ == "__main__":
    plot_optimal_ctdc_search()
